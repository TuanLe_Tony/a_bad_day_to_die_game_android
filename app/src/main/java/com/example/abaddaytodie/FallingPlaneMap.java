package com.example.abaddaytodie;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.Location;
import android.os.CountDownTimer;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

public class FallingPlaneMap extends AppCompatActivity implements OnMapReadyCallback {

    FirebaseDatabase database;
    DatabaseReference root;
    TextView miles,notifycation,countdown, endCountdown;

    List<String> PlayersName = new ArrayList<String>();

    private ChildEventListener mChildEventListener ;
    private String randomGAME[] =
            {"Plane"};

    //"Shaking", "Math",


    // permissions variables
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private boolean mLocationPermissionGranted;

    // map variables
    private GoogleMap mMap;
    private static final int DEFAULT_ZOOM = 15;


    // location variables
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Location mLastKnownLocation;    // location of device
    private LocationCallback mLocationCallback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_falling_plane_map);

        notifycation = (TextView)findViewById(R.id.Notify);
        countdown = (TextView) findViewById(R.id.countdown);
        endCountdown = (TextView) findViewById(R.id.endgame);


        //240 secound countdown
        new CountDownTimer(240000, 1000) {

            public void onTick(long millisUntilFinished) {
                countdown.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                // go to PlayerDetails screen
                Intent myIntent = new Intent(FallingPlaneMap.this, PlaneDeath.class);
                startActivity(myIntent);
            }
        }.start();

        //GET NAME THAT WAS SAVED FROM MainActivity :)
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor Edit = pref.edit();

        final String name = pref.getString("NAME", "NOTHING");         // getting String

        database = FirebaseDatabase.getInstance();
        root = database.getReference();

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        // ask for permission
        Log.d("Tony", "onCreate - asking for permissions");
        getLocationPermission();


        // map variable setup
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync( this);


        FirebaseDatabase.getInstance().getReference().child("Players").child(name);

        miles = (TextView)findViewById(R.id.miles);

    }

    // MARK - Map UI functions
    public void onMapReady(GoogleMap map) {
        mMap = map;

        // Do other setup activities here too, as described elsewhere in this tutorial.
        UiSettings settings = mMap.getUiSettings();
        settings.setZoomControlsEnabled(true);

        // Ask for permissions
        Log.d("Tony", "is permission given? " + mLocationPermissionGranted);

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();



        // Get the current location of the device and set the position of the map.
        getDeviceLocation();

        Log.d("Tony", "going to setup callback");
        mLocationCallback = setupLocationCallback();


    }




    private LocationCallback setupLocationCallback() {
        LocationCallback mLocationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                Log.d("Tony", "+++++ CALLING CALLBACK");
                for (Location location : locationResult.getLocations()) {
                    Log.d("Tony", "Location: " + location.getLatitude() + " " + location.getLongitude());
                    mLastKnownLocation = location;

                    // @TODO: create a global variable to track the current marker
                    /*
                    if (mCurrLocationMarker != null) {
                        mCurrLocationMarker.remove();
                    }
                    */

                    // @TODO: remove this when you're finished
                    mMap.clear();

                    //Place current location marker
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    markerOptions.title("Current Position");
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                    //mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);

                    mMap.addMarker(markerOptions);

                    //move map camera
                    //mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
                }
            };

        };

        return mLocationCallback;
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {

                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);


            } else {
                Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
                Log.d("Tony", "Permission denied, ask for permission again");
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void getDeviceLocation() {

        //GET NAME THAT WAS SAVED FROM MainActivity :)
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor Edit = pref.edit();

        final String name = pref.getString("NAME", "NOTHING");         // getting String

        //get a random value string
        Random r = new Random();
        final String random = (randomGAME[new Random().nextInt(randomGAME.length)]);

        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Log.d("Tony", "getDeviceLocation - got permission");
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();

                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {

                            Log.d("Tony", "Got last known location");

                            mLastKnownLocation = task.getResult();

                            // Set the map's camera position to the current location of the device.
                            if (mLastKnownLocation == null) {
                                Log.d("Tony", "Last known location is null");
                                Toast.makeText(FallingPlaneMap.this, "Location is null", Toast.LENGTH_SHORT).show();

                            }
                            else {
                                // Just show the lat and long stuff here (It's Annoying so commented it )
//                                String loc = String.valueOf(mLastKnownLocation.getLatitude() + "," + mLastKnownLocation.getLongitude());
//                                Toast.makeText(MapshowPlayer.this, loc , Toast.LENGTH_SHORT).show();
//                                Log.d("Tony", mLastKnownLocation.toString());

                                //send lat and lg to database =.=
                                root.child("Users").child(name).child("lat").setValue(Double.toString(mLastKnownLocation.getLatitude()));
                                root.child("Users").child(name).child("lg").setValue(Double.toString(mLastKnownLocation.getLongitude()));
                                root.child("Users").child(name).child("name").setValue(name);


                                // lat & long of current position
                                LatLng currentPosition = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());

                                //GET PLANE LAT AND LONG
                                if (mChildEventListener == null) {

                                    mChildEventListener = new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                            final ShowUsers users = dataSnapshot.getValue(ShowUsers.class);
                                            String name = users.getName();

                                            PlayersName.add(name);

                                            // create a default map marker and put it here
                                            LatLng plane = new LatLng(new Double(users.getLat()), new Double(users.getLg()));

                                            mMap.addMarker(new MarkerOptions().position(plane).
                                                    icon(BitmapDescriptorFactory.fromBitmap(createCustomMarker(FallingPlaneMap.this,R.drawable.plane,"Plane")))).setTitle(random);

                                        }


                                        @Override
                                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                                        }

                                        @Override
                                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    };
                                    root.child(name+"plane").addChildEventListener(mChildEventListener);
                                }


                                mMap.addMarker(new MarkerOptions().position(currentPosition).
                                        icon(BitmapDescriptorFactory.fromBitmap(createCustomMarker(FallingPlaneMap.this,R.drawable.tony,"Tony")))).setTitle(name);

                                // move to current location
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentPosition, DEFAULT_ZOOM));

                                //If player click marker do something :)
                                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() //click marker
                                {

                                    @Override
                                    public boolean onMarkerClick(Marker map) {

                                        //GET NAME THAT WAS SAVED FROM MainActivity :)
                                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                        SharedPreferences.Editor Edit = pref.edit();

                                        String LAT = pref.getString("LAT", "NOTHING");         // getting String
                                        String LONG = pref.getString("LONG", "NOTHING");         // getting String

                                        if (map.getTitle().equals("Plane")) {
                                            //send lat and lg to database =.=
                                            root.child("Users").child(name).child("lat").setValue(Double.toString(mLastKnownLocation.getLatitude()));
                                            root.child("Users").child(name).child("lg").setValue(Double.toString(mLastKnownLocation.getLongitude()));
//
                                            // caculated distance between 2 locations
                                            Location selected_location=new Location("locationA");
                                            selected_location.setLatitude(mLastKnownLocation.getLatitude());
                                            selected_location.setLongitude(mLastKnownLocation.getLongitude());
                                            //test
//                                            selected_location.setLatitude(43.7725904);
//                                            selected_location.setLongitude(-79.3255643);
                                            Location near_locations=new Location("locationB");
                                            near_locations.setLatitude(Double.valueOf(LAT));
                                            near_locations.setLongitude(Double.valueOf(LONG));


                                            double distance=selected_location.distanceTo(near_locations);

                                            miles.setText("SAFE IN 100 - CURRENT IN: " + (int) distance);

                                            if(distance >= 100){
                                                Toast.makeText(FallingPlaneMap.this, "You're in the safe-zone - YOU WIN", Toast.LENGTH_SHORT).show();
                                                //GET SCORE THAT WAS SAVED FROM PlayerDetails :)
                                                SharedPreferences pref1 = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                                String score = pref1.getString("SCORE", "NOTHING");         // getting String
                                                //UPDATE NEW SCORE
                                                Integer updateScore = (Integer.valueOf(score) + 10);

                                                //add NEW SCORE to database
                                                root.child("Players").child(name).child("Score").setValue(String.valueOf(updateScore));

                                                //3 secound countdown
                                                new CountDownTimer(3000, 1000) {

                                                    public void onTick(long millisUntilFinished) {
                                                        endCountdown.setText("" + millisUntilFinished / 1000);
                                                    }

                                                    public void onFinish() {
                                                        // go to PlayerDetails screen
                                                        Intent myIntent = new Intent(FallingPlaneMap.this, PlayerDetails.class);
                                                        startActivity(myIntent);
                                                    }
                                                }.start();
                                            }
                                            else{
                                                Toast.makeText(FallingPlaneMap.this, "DANGER! DANGER! - Keep running", Toast.LENGTH_SHORT).show();
                                            }

                                            return true;

                                        }
                                        else if (PlayersName.contains(map.getTitle())) {
                                            Toast.makeText(FallingPlaneMap.this, map.getTitle(), Toast.LENGTH_SHORT).show();
                                            return true;
                                        }
                                        else {
                                            Toast.makeText(FallingPlaneMap.this, "not exist", Toast.LENGTH_SHORT).show();
                                            return false;
                                        }

                                    }
                                });
                            }


                        } else {
                            Log.d("Tony", "Current location is null. Using defaults.");
                            Log.e("Tony", "Exception: %s", task.getException());

                            // create a default map marker and put it here
                            LatLng sydney = new LatLng(-33.87365, 151.20689);
                            mMap.addMarker(new MarkerOptions().position(sydney)
                                    .title("Marker in Sydney"));
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, DEFAULT_ZOOM));

                            //mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
            else {
                Log.d("Tony", "getDeviceLocation - permission denied, not doing anything!");
            }
        } catch(SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }
    // MARK - Permissions functions
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Log.d("Tony", "getLocationPermission - Permission Granted");
            mLocationPermissionGranted = true;
        } else {
            Log.d("Tony", "getLocationPermission - Permission Denied");
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {

        Log.d("Tony", "inside onRequestPermissionResult");
        Log.d("Tony", String.valueOf(requestCode));
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }

        //TODO: add this functions
        updateLocationUI();
    }
    // setting image bitmap =.=
    public static Bitmap createCustomMarker(Context context, @DrawableRes int resource, String _name) {

        View marker = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker_layout, null);

        CircleImageView markerImage = (CircleImageView) marker.findViewById(R.id.user_dp);
        markerImage.setImageResource(resource);
        TextView txt_name = (TextView)marker.findViewById(R.id.name);
        txt_name.setText(_name);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        marker.setLayoutParams(new ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT));
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        marker.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(marker.getMeasuredWidth(), marker.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        marker.draw(canvas);
        return bitmap;
    }

    //exit from app when press back button
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                }).setNegativeButton("No", null).show();
    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
