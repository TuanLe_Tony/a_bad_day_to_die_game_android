package com.example.abaddaytodie;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.constraint.solver.widgets.Snapshot;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PlayerDetails extends AppCompatActivity {
    private static final String PREFS_NAME = "";
    private static final String MyPREFERENCES = "";
    TextView StoredName,Scores;
    Button challenge;


    private URI mImageUri;



    static final int REQUEST_IMAGE_CAPTURE = 1;

    ImageView mImageView;

    FirebaseDatabase database;
    DatabaseReference root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_details);

        mImageView=(ImageView)findViewById(R.id.ImageView);


        //GET NAME THAT WAS SAVED FROM MainActivity :)
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor Edit = pref.edit();

        final String name = pref.getString("NAME", "NOTHING");         // getting String

        StoredName = (TextView)findViewById(R.id.PlayernameTV);
        StoredName.setText(name); //set the name to textview

        Log.d("Name saved", name);

        database = FirebaseDatabase.getInstance();
        root = database.getReference();

        root.child("Message").child(name).child("message").setValue(0);

        challenge = (Button)findViewById(R.id.challenge);

        Button tutorial = (Button)findViewById(R.id.tutorial);


        tutorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PlayerDetails.this, Tutorial.class));

            }
        });

//        // Fetch particular child -> child -> etc =.=
        final DatabaseReference Players = FirebaseDatabase.getInstance().getReference("Players");
        final DatabaseReference playerName = Players.child(name); //skip Score and get value.
        Scores = (TextView)findViewById(R.id.ScoreTV);
        playerName.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot zoneSnapshot: dataSnapshot.getChildren()) {
                    String Score = zoneSnapshot.getValue(String.class);

                    //SAVE INPUT NAME
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                    SharedPreferences.Editor Edit = pref.edit();

                    Edit.putString("SCORE", Score);  // Saving string
                    Edit.commit(); // commit changes


                    Scores.setText("SCORE: " + Score); //set the name to textview
                    if (Integer.valueOf(Score) == 100){

                        ImageView NewRank = (ImageView) findViewById(R.id.RankImage);
                        NewRank.setImageResource(R.drawable.gold5);
                    }
                    if (Integer.valueOf(Score) == 0){
                        ImageView NewRank = (ImageView) findViewById(R.id.RankImage);
                        NewRank.setImageResource(R.drawable.bronze5);
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                System.out.print("ERROR");
            }
        });


        //GET MESSAGE
        //        // Fetch particular child -> child -> etc =.=
        final DatabaseReference message = FirebaseDatabase.getInstance().getReference("Message");
        final DatabaseReference myName = message.child(name); //skip Score and get value.
        myName.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot zone: dataSnapshot.getChildren()) {
                    Integer Message = zone.getValue(Integer.class);


                    if (Message == 0){
                        challenge.setVisibility(View.INVISIBLE);
                    }
                    else if (Message == 1) {
                        challenge.setText("YES");
                        challenge.setVisibility(View.VISIBLE);
                        challenge.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(PlayerDetails.this, WaitingRoom1.class));
                                root.child("ShipGame").child("player").child("player2").setValue(name);
                            }
                        });
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.print("ERROR");
            }
        });



        Button btn = (Button)findViewById(R.id.StartButton);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //DIALOG.
//                ProgressDialog dialog = ProgressDialog.show(PlayerDetails.this, "",
//                        "Loading. Please wait...", true);

                startActivity(new Intent(PlayerDetails.this, MapShowPlayer.class));

            }
        });
    }
//exit from app when press back button
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                }).setNegativeButton("No", null).show();
    }

    //get IMAGE


    public void SelectPhotoMethod(View view) {
        Intent takePictureIntent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takePictureIntent.resolveActivity(getPackageManager())!=null){
            startActivityForResult(takePictureIntent,REQUEST_IMAGE_CAPTURE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==REQUEST_IMAGE_CAPTURE && resultCode==RESULT_OK){
            Bundle extras=data.getExtras();
            Bitmap imageBitmap=(Bitmap) extras.get("data");
            mImageView.setImageBitmap(imageBitmap);
            try {
                createImageFile();
                save();

            }catch (Exception ex){
                ex.printStackTrace();
            }

        }
    }

    String mCurrentPhotpPath;
    //create image name
    private File createImageFile() throws IOException {
        String timeStamp=new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName="JPGE"+timeStamp+"_";
        File storgeDir= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image=File.createTempFile(imageFileName,".jpg",storgeDir);
        mCurrentPhotpPath="file:"+image.getAbsolutePath();
        return image;
    }
    private File createTemporaryFile(String part, String ext) throws Exception
    {
        File tempDir= Environment.getExternalStorageDirectory();
        tempDir=new File(tempDir.getAbsolutePath()+"/.temp/");
        if(!tempDir.exists())
        {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }



    //save image
    private void save(){
        Intent mediaScanIntent=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f=new File(mCurrentPhotpPath);
        Uri contentUri=Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);

    }

}

