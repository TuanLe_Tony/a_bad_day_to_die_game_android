package com.example.abaddaytodie;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.VideoView;

public class shakedeath extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shakedeath);

        VideoView view = (VideoView)findViewById(R.id.video1);
        String path = "android.resource://" + getPackageName() + "/" + R.raw.shakingdealth;
        view.setVideoURI(Uri.parse(path));
        view.start();


        //30 secound countdown
        new CountDownTimer(46000, 1000) {

            public void onTick(long millisUntilFinished) {
                Log.d("dealth countdown finish","" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                // go to PlayerDetails screen
                Intent myIntent = new Intent(shakedeath.this, PlayerDetails.class);
                startActivity(myIntent);
            }
        }.start();
    }
    //exit from app when press back button
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                }).setNegativeButton("No", null).show();
    }
}
