package com.example.abaddaytodie;

import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;

public class InteractGAME1 extends AppCompatActivity {

    GameEngine1 tappySpaceship1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get size of the screen
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        // Initialize the GameEngine object
        // Pass it the screen size (height & width)
        tappySpaceship1 = new GameEngine1(this, size.x, size.y);

        // Make GameEngine the view of the Activity
        setContentView(tappySpaceship1);
    }

    // Android Lifecycle function
    @Override
    protected void onResume() {
        super.onResume();
        tappySpaceship1.startGame();
    }

    // Stop the thread in snakeEngine
    @Override
    protected void onPause() {
        super.onPause();
        tappySpaceship1.pauseGame();
    }
}