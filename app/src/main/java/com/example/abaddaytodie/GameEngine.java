package com.example.abaddaytodie;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {
    private ChildEventListener mChildEventListener;

    FirebaseDatabase database;
    DatabaseReference root;

    List<String> playersArray = new ArrayList<String>();


    // Android debug variables
    final static String TAG = "TAPPY-SPACESHIP";

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    // -----------------------------------
    // GAME SPECIFIC VARIABLES
    // -----------------------------------

    // ----------------------------
    // ## SPRITES
    // ----------------------------
    Player player;
    Enemy enemy;
    Bullet bullet;
    Bullet1 bullet1;
    Egg1 egg1;
    Egg egg;

    int EggDestroy = 0;


    public GameEngine(Context context, int w, int h) {
        super(context);
        database = FirebaseDatabase.getInstance();
        root = database.getReference();


        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;


        this.printScreenInfo();

        // @TODO: Add your sprites
        this.spawnPlayer();
        this.spawnEnemyShips();
        this.spawnBullet();
        this.spawnBullet1();
        this.spawnEgg1();
        this.spawnEgg();
        // @TODO: Any other game setup

    }


    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        // put player in middle of screen --> you may have to adjust the Y position
        // depending on your device / emulator
        player = new Player(this.getContext(), this.screenWidth/6, (this.screenHeight-300) / 2);
        root.child("GAME").child("RIGHT").setValue((this.screenHeight-300) / 2);
    }
    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location
        enemy = new Enemy(this.getContext(), this.screenWidth/2 + this.screenWidth/4, (this.screenHeight-300) / 2);
    }

    private void spawnBullet() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location
        bullet = new Bullet(this.getContext(), this.screenWidth/6, (this.screenHeight-280) / 2 + 120);
        root.child("ROCKET").child("RIGHT").setValue(this.screenWidth/6);
    }
    private void spawnBullet1() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location
        bullet1 = new Bullet1(this.getContext(), this.screenWidth/2 + this.screenWidth/4, (this.screenHeight-280) / 2 + 120);
    }
    private void spawnEgg1(){
        egg1 = new Egg1(this.getContext(), this.screenWidth - this.screenWidth/18, (0));
    }
    private void spawnEgg(){

        egg = new Egg(this.getContext(), this.screenWidth/18, this.screenHeight - this.screenHeight/18);
        root.child("EGG").child("RIGHT").setValue(this.screenHeight - this.screenHeight/18);
    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {
        // @TODO: Update position of player
        player.updatePlayerPosition();
        // @TODO: Update position of enemy ships
        enemy.updateEnemyPosition();
        // @TODO: Update position of enemy ships
        bullet1.updateBulletPosition();
        bullet.updateBulletPosition();
        egg1.updateEggPosition();
        egg.updateEggPosition();
        // @TODO: Collision detection between enemy and wall

        this.bullet.setDirection(1);
        this.egg.setDirection(1);

        if (bullet.getXPosition() >= (this.screenWidth + 100)) {
            bullet.setXPosition(290);
            root.child("ROCKET").child("RIGHT").setValue(290);
            bullet.setYPosition(player.getYPosition()+120);
        }
        if (bullet1.getXPosition() <= (-100)) {
            bullet1.setXPosition(1500);
            bullet1.setYPosition(enemy.getYPosition()+120);
        }
        if (egg1.getYPosition() >= this.screenHeight){
            egg1.setYPosition(0);
        }
        if (egg.getYPosition() <= 0){
            egg.setYPosition(this.screenHeight - this.screenHeight/18);
            root.child("EGG").child("RIGHT").setValue(this.screenHeight - this.screenHeight/18);
        }
        // @TODO: Collision detection between player and enemy
        if (bullet.getHitbox().intersect(egg1.getHitbox())) {
            // reduce lives
            EggDestroy++;
            // reset player to original position
            root.child("ROCKET").child("RIGHT").setValue(290);
            bullet.setYPosition(player.getYPosition()+120);
        }
        // @TODO: Collision detection between player and enemy
        if (bullet.getHitbox().intersect(enemy.getHitbox())) {

            // draw game stats
            paintbrush.setTextSize(60);
            paintbrush.setColor(Color.BLACK);
            canvas.drawText("Blocked", this.screenWidth/6, this.player.getYPosition(), paintbrush);

            root.child("ROCKET").child("RIGHT").setValue(290);
            bullet.setYPosition(player.getYPosition()+120);
        }
        // @TODO: Collision detection between player and enemy
        if (bullet1.getHitbox().intersect(egg.getHitbox())) {

            // reset player to original position;
            bullet1.setYPosition(enemy.getYPosition()+120);
        }
        // @TODO: Collision detection between player and enemy
        if (bullet1.getHitbox().intersect(player.getHitbox())) {

            // draw game stats
            paintbrush.setTextSize(60);
            paintbrush.setColor(Color.BLACK);
            canvas.drawText( "Blocked", this.screenWidth/2 + this.screenWidth/4, this.screenHeight - this.screenHeight/14, paintbrush);//---------------
            bullet1.setYPosition(enemy.getYPosition()+120);
        }

        if (EggDestroy == 5){
            root.child("WIN").child("win").setValue(1);
            root.child("LOST1").child("lost").setValue(1);
        }


            // WIN CHECKING DATABASE

        final DatabaseReference WIN = FirebaseDatabase.getInstance().getReference("WIN");


        WIN.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot zoneSnapshot: dataSnapshot.getChildren()) {
                    Integer win = zoneSnapshot.getValue(Integer.class);

                    if (win == 1){
                        Log.d("GAME","LOST");

//                        //When lost all lives go to GameOver screen.
//                        Intent intent1 = new Intent(getContext(), GameWin.class);
//                        getContext().startActivity(intent1);

                    }
                    else {
                        Log.d("ERROR","GOT AN EROOR");
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.print("ERROR");
            }
        });

        // WIN CHECKING DATABASE

        final DatabaseReference LOST = FirebaseDatabase.getInstance().getReference("LOST");


        LOST.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot zoneSnapshot: dataSnapshot.getChildren()) {
                    Integer lost = zoneSnapshot.getValue(Integer.class);

                    if (lost == 1){
                        Log.d("GAME","LOST");
//                        //When lost all lives go to GameOver screen.
//                        Intent intent = new Intent(getContext(), GameOver.class);
//                        getContext().startActivity(intent);

                    }
                    else {
                        Log.d("ERROR","GOT AN EROOR");
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.print("ERROR");
            }
        });

    }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.WHITE);


            //@TODO: Draw the player
            canvas.drawBitmap(this.player.getBitmap(), this.player.getXPosition(), this.player.getYPosition(), paintbrush);

            //@TODO: Draw the enemy
            canvas.drawBitmap(this.enemy.getBitmap(), this.enemy.getXPosition(), this.enemy.getYPosition(), paintbrush);

            //@TODO: Draw the bullet
            canvas.drawBitmap(this.bullet.getBitmap(), this.bullet.getXPosition(), this.bullet.getYPosition(), paintbrush);

            //@TODO: Draw the bullet1
            canvas.drawBitmap(this.bullet1.getBitmap(), this.bullet1.getXPosition(), this.bullet1.getYPosition(), paintbrush);
            //@TODO: Draw the egg1
            canvas.drawBitmap(this.egg1.getBitmap(), this.egg1.getXPosition(), this.egg1.getYPosition(), paintbrush);
            //@TODO: Draw the egg
            canvas.drawBitmap(this.egg.getBitmap(), this.egg.getXPosition(), this.egg.getYPosition(), paintbrush);


            // Show the hitboxes on player and enemy
            paintbrush.setColor(Color.WHITE);
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(-1);

            Rect eggtHitbox = egg.getHitbox();
            canvas.drawRect(eggtHitbox.left, eggtHitbox.top, eggtHitbox.right, eggtHitbox.bottom, paintbrush);

            Rect eggtHitbox1 = egg1.getHitbox();
            canvas.drawRect(eggtHitbox1.left, eggtHitbox1.top, eggtHitbox1.right, eggtHitbox1.bottom, paintbrush);

            Rect bulletHitbox = bullet1.getHitbox();
            canvas.drawRect(bulletHitbox.left, bulletHitbox.top, bulletHitbox.right, bulletHitbox.bottom, paintbrush);

            Rect bulletHitbox1 = bullet.getHitbox();
            canvas.drawRect(bulletHitbox1.left, bulletHitbox1.top, bulletHitbox1.right, bulletHitbox1.bottom, paintbrush);

            Rect playerHitbox = player.getHitbox();
            canvas.drawRect(playerHitbox.left, playerHitbox.top, playerHitbox.right, playerHitbox.bottom, paintbrush);

            Rect enemyHitbox = enemy.getHitbox();
            canvas.drawRect(enemyHitbox.left, enemyHitbox.top, enemyHitbox.right, enemyHitbox.bottom, paintbrush);


            // draw game stats
            paintbrush.setTextSize(60);
            paintbrush.setColor(Color.BLACK);
            canvas.drawText("Destroyed: " + EggDestroy, this.screenWidth/6, 50, paintbrush);

            //----------------
            this.holder.unlockCanvasAndPost(canvas);
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(50);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();


// If u can pass or get key-data this method will use for 1 GameEngine.(Verify ship for each player)
        String name = "Tony le";


        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {

            //GET USERS LAT AND LONG
            if (mChildEventListener == null) {

                mChildEventListener = new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        ShowPlayers players = dataSnapshot.getValue(ShowPlayers.class);
                        String user1 = players.getPlayer1();
                        String user2 = players.getPlayer2();

                        playersArray.add(user1);
                        playersArray.add(user2);

                    }


                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                };
                root.child("ShipGame").addChildEventListener(mChildEventListener);
            }
            // move player up
            int x = (int)event.getX();
            int y = (int)event.getY();

            if (this.screenHeight/2 < y){
                if (name == "Tony le") {
                    this.player.setDirection(1);
                }

            }
            if (this.screenHeight/2 > y){
                if (name == "Tony le") {
                    this.player.setDirection(0);
                }
            }

        }
        else if (userAction == MotionEvent.ACTION_UP) {
            // move player down
            this.player.setDirection(2);

        }

        return true;
    }
}