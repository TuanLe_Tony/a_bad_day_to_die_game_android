package com.example.abaddaytodie;

public class ShowPlayers {

    public String player1;
    public String player2;


    public ShowPlayers() {

    }
    public ShowPlayers(String player1, String player2) {
        this.player1 = player1;
        this.player2 = player2;
    }
    public String getPlayer1() {
        return this.player1;
    }

    public String getPlayer2() {
        return this.player2;
    }


    public void setPlayer1(String n) {
        this.player1 = n;
    }
    public void setPlayer2(String n) {
        this.player2 = n;
    }
}
