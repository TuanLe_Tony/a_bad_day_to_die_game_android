package com.example.abaddaytodie;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {
    private static final String PREFS_NAME = "";

    FirebaseDatabase database;
    DatabaseReference root;

    EditText NameInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Give your SharedPreferences file a name and save it to a static variable
        final String PREFS_NAME = "MyPrefsFile";

        // database
        database = FirebaseDatabase.getInstance();
        root = database.getReference();


        SharedPreferences settings = getSharedPreferences(MainActivity.PREFS_NAME, 0);
        //Get "hasLoggedIn" value. If the value doesn't exist yet false is returned
        boolean hasLoggedIn = settings.getBoolean("hasLoggedIn", false);

        if(hasLoggedIn)
        {

            Intent intent = new Intent();
            intent.setClass(MainActivity.this, PlayerDetails.class);
            startActivity(intent);
            MainActivity.this.finish();
        }


        Button btn = (Button)findViewById(R.id.Button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NameInput = (EditText)findViewById(R.id.EditName);
                final String PlayerName = NameInput.getText().toString();

                if(PlayerName.isEmpty()){

                    Log.d("Tuan", "Filed Blank");
                    Toast.makeText(MainActivity.this, "Please, Field in",Toast.LENGTH_LONG).show();
                }

                // database check if child exist then do something
                final DatabaseReference root = FirebaseDatabase.getInstance().getReference();
                DatabaseReference users = root.child("Players");
                users.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot Thesnapshot) {
                        if (Thesnapshot.hasChild(PlayerName)) {
                            Log.d("Tuan","KO dc dau");
                            Toast.makeText(MainActivity.this, "Sorry, The Name has been taken.",Toast.LENGTH_SHORT).show();


                        }// end if go to else
                        else {

                            // We need an Editor object to make preference changes.
                            SharedPreferences settings = getSharedPreferences(MainActivity.PREFS_NAME, 0);
                            SharedPreferences.Editor editor = settings.edit();

                            //Set "hasLoggedIn" to true
                            editor.putBoolean("hasLoggedIn", true);

                            // Commit the edits!
                            editor.commit();

                            //SAVE INPUT NAME
                            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                            SharedPreferences.Editor Edit = pref.edit();

                            NameInput = (EditText)findViewById(R.id.EditName);
                            final String Player = NameInput.getText().toString();

                            Edit.putString("NAME", Player);  // Saving string
                            Edit.commit(); // commit changes

                            //add to database
                            root.child("Players").child(PlayerName).child("Score").setValue("0");

                            // go to next screen
                            Intent myIntent = new Intent(MainActivity.this, PlayerDetails.class);
                            startActivity(myIntent);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d("Tuan","KO dc dau");
                        Toast.makeText(MainActivity.this, "Sorry, It's on Cacelled.",Toast.LENGTH_SHORT).show();



                    }
                });

            }
        });


    }
}
