package com.example.abaddaytodie;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.seismic.ShakeDetector;

import java.util.Random;

public class ShakingGame extends AppCompatActivity implements ShakeDetector.Listener {
    FirebaseDatabase database;
    DatabaseReference root;

    int timer = 3;
    TextView endCountdown;
    TextView Notification;
    private String Fruits[] =
            {"apple", "oranger", "pear", "strawberry", "wetermelon"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shaking_game);

        // database
        database = FirebaseDatabase.getInstance();
        root = database.getReference();

        //1. create a bunch of nonsense variables
        //this  a mandatory funcion
        // it's required bt the ShakeDetector.listener class
        SensorManager manager = (SensorManager) getSystemService(SENSOR_SERVICE);
        ShakeDetector detector = new ShakeDetector((ShakeDetector.Listener) this);
        detector.start(manager);

        endCountdown = (TextView)findViewById(R.id.endgame);



    }


    String currentChoice;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void hearShake() {

        //GET NAME THAT WAS SAVED FROM MainActivity :)
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor Edit = pref.edit();

        final String name = pref.getString("NAME", "NOTHING");         // getting String

        //2. this function AUTOAGICALL gets called
        //every time you shake the phone
        Random r = new Random();
        String random = (Fruits[new Random().nextInt(Fruits.length)]);



        try {


            if (timer == 3) {
                Notification = (TextView)findViewById(R.id.TV2);
                ImageView NewFruit = (ImageView) findViewById(R.id.iv);
                if (random == "apple") {
                    NewFruit.setImageResource(R.drawable.apple1);
                    Notification.setText("YOU WON!!");

                    String score = pref.getString("SCORE", "NOTHING");         // getting String
                    //UPDATE NEW SCORE
                    Integer updateScore = (Integer.valueOf(score) + 10);

                    //add NEW SCORE to database
                    root.child("Players").child(name).child("Score").setValue(String.valueOf(updateScore));

                    //3 secound countdown
                    new CountDownTimer(3000, 1000) {

                        public void onTick(long millisUntilFinished) {
                            endCountdown.setText("" + millisUntilFinished / 1000);
                        }

                        public void onFinish() {
                            // go to next screen
                            Intent myIntent = new Intent(ShakingGame.this, PlayerDetails.class);
                            startActivity(myIntent);
                        }
                    }.start();
                }
                if (random == "oranger") {
                    NewFruit.setImageResource(R.drawable.oranger);
                }
                if (random == "pear") {
                    NewFruit.setImageResource(R.drawable.pear);
                }
                if (random == "strawberry") {
                    NewFruit.setImageResource(R.drawable.strawberry);
                }
                if (random == "wetermelon") {
                    NewFruit.setImageResource(R.drawable.wetermelon);
                }


                Toast.makeText(this, "Shaking 1 time", Toast.LENGTH_SHORT).show();
                timer = timer - 1;

            } else if (timer == 2) {
                Notification = (TextView)findViewById(R.id.TV2);
                ImageView NewRank = (ImageView) findViewById(R.id.iv);


                ImageView NewFruit = (ImageView) findViewById(R.id.iv);
                if (random == "apple") {
                    NewFruit.setImageResource(R.drawable.apple1);
                    Notification.setText("YOU WON!!");

                    //3 secound countdown
                    new CountDownTimer(3000, 1000) {

                        public void onTick(long millisUntilFinished) {
                            endCountdown.setText("" + millisUntilFinished / 1000);
                        }

                        public void onFinish() {
                            // go to next screen
                            Intent myIntent = new Intent(ShakingGame.this, PlayerDetails.class);
                            startActivity(myIntent);
                        }
                    }.start();
                }
                if (random == "oranger") {
                    NewFruit.setImageResource(R.drawable.oranger);
                }
                if (random == "pear") {
                    NewFruit.setImageResource(R.drawable.pear);
                }
                if (random == "strawberry") {
                    NewFruit.setImageResource(R.drawable.strawberry);
                }
                if (random == "wetermelon") {
                    NewFruit.setImageResource(R.drawable.wetermelon);
                }

                Toast.makeText(this, "Shaking 2 times", Toast.LENGTH_SHORT).show();
                timer = timer - 1;



            } else if (timer == 1) {
                Notification = (TextView)findViewById(R.id.TV2);
                ImageView NewRank = (ImageView) findViewById(R.id.iv);

                ImageView NewFruit = (ImageView) findViewById(R.id.iv);
                if (random == "apple") {
                    NewFruit.setImageResource(R.drawable.apple1);
                    Notification.setText("YOU WON!!");
                    //3 secound countdown
                    new CountDownTimer(3000, 1000) {

                        public void onTick(long millisUntilFinished) {
                            endCountdown.setText("" + millisUntilFinished / 1000);
                        }

                        public void onFinish() {
                            // go to next screen
                            Intent myIntent = new Intent(ShakingGame.this, PlayerDetails.class);
                            startActivity(myIntent);
                        }
                    }.start();
                }
                if (random == "oranger") {
                    NewFruit.setImageResource(R.drawable.oranger);
                }
                if (random == "pear") {
                    NewFruit.setImageResource(R.drawable.pear);
                }
                if (random == "strawberry") {
                    NewFruit.setImageResource(R.drawable.strawberry);
                }
                if (random == "wetermelon") {
                    NewFruit.setImageResource(R.drawable.wetermelon);
                }
                timer = timer - 1;
                Toast.makeText(this, "Shaking 3 times", Toast.LENGTH_SHORT).show();


            } else if (timer == 0) {

                Toast.makeText(this, "You Lost", Toast.LENGTH_SHORT).show();
                //3 secound countdown
                new CountDownTimer(3000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        endCountdown.setText("" + millisUntilFinished / 1000);
                    }

                    public void onFinish() {
                        // go to next screen
                        Intent myIntent = new Intent(ShakingGame.this, shakedeath.class);
                        startActivity(myIntent);
                    }
                }.start();

            }
        } catch (Exception e) {
            Log.d("sakhi", "Error while turning on camera flash");
            Toast.makeText(this, "Error while turning on camera flash", Toast.LENGTH_SHORT).show();

        }

    }
    //exit from app when press back button
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                }).setNegativeButton("No", null).show();
    }
}