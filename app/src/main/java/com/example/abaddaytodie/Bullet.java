package com.example.abaddaytodie;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.example.abaddaytodie.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Bullet {

    FirebaseDatabase database;
    DatabaseReference root;

    int xPosition;
    int yPosition;
    int direction = -1;          // 0 = down, 1 = up
    Bitmap rocket;
    int Speed = 30;

    private Rect hitBox;

    public Bullet(Context context, int x, int y) {
        database = FirebaseDatabase.getInstance();
        root = database.getReference();

        // Fetch particular child -> child -> etc =.=
        final DatabaseReference rocket = FirebaseDatabase.getInstance().getReference("ROCKET");
        rocket.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot zoneSnapshot: dataSnapshot.getChildren()) {
                    Integer theY = zoneSnapshot.getValue(Integer.class);
                    //set data from database to game position
                    xPosition = theY;
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.print("ERROR");
            }
        });

        this.rocket = BitmapFactory.decodeResource(context.getResources(), R.drawable.rocket1);
        this.xPosition = x;
        this.yPosition = y;
        this.hitBox = new Rect(this.xPosition, this.yPosition, this.xPosition + this.rocket.getWidth(), this.yPosition + this.rocket.getHeight());
    }
    public void updateBulletPosition() {
        if (this.direction == 1) {
            // move right
            this.xPosition = this.xPosition + Speed;
            root.child("ROCKET").child("RIGHT").setValue(this.xPosition = this.xPosition + Speed);
        }
        // update the position of the hitbox
        this.updateHitbox();
    }

    public void updateHitbox() {
        // update the position of the hitbox
        this.hitBox.top = this.yPosition;
        this.hitBox.left = this.xPosition;
        this.hitBox.right = this.xPosition + this.rocket.getWidth();
        this.hitBox.bottom = this.yPosition + this.rocket.getHeight();
    }

    public Rect getHitbox() {
        return this.hitBox;
    }

    public void setXPosition(int x) {
        this.xPosition = x;
        this.updateHitbox();
    }
    public void setYPosition(int y) {
        this.yPosition = y;
        this.updateHitbox();
    }
    public int getXPosition() {
        return this.xPosition;
    }
    public int getYPosition() {
        return this.yPosition;
    }

    public void setDirection(int i) {
        this.direction = i;
    }
    public Bitmap getBitmap() {
        return this.rocket;
    }

}
