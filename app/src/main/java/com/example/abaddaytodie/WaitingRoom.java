package com.example.abaddaytodie;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class WaitingRoom extends AppCompatActivity {
    FirebaseDatabase database;
    DatabaseReference root;
    private ChildEventListener mChildEventListener ;

    TextView textView,endCountdown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_room);

        database = FirebaseDatabase.getInstance();
        root = database.getReference();

        textView = (TextView)findViewById(R.id.PlayerName);
        endCountdown = (TextView)findViewById(R.id.endgame);

        root.child("ShipGame-Start").child("Start").child("value").setValue(0);
        root.child("WIN").child("win").setValue(0);
        root.child("LOST").child("lost").setValue(0);


        //GET USERS LAT AND LONG
        if (mChildEventListener == null) {

            mChildEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    ShowPlayers players = dataSnapshot.getValue(ShowPlayers.class);
                    String user1 = players.getPlayer1();
                    String user2 = players.getPlayer2();

                    textView.setText(user1 + "\n" + user2);
                }


                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            root.child("ShipGame").addChildEventListener(mChildEventListener);
        }

        Button btn = (Button)findViewById(R.id.start);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                root.child("ShipGame-Start").child("Start").child("value").setValue(1);

            }
        });

        //GET MESSAGE
        //        // Fetch particular child -> child -> etc =.=
        final DatabaseReference game = FirebaseDatabase.getInstance().getReference("ShipGame-Start");
        final DatabaseReference start = game.child("Start"); //skip Score and get value.
        start.addValueEventListener(new ValueEventListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot zone: dataSnapshot.getChildren()) {
                    Integer gameStart = zone.getValue(Integer.class);


                    if (gameStart == 0){

                    }
                    else if (gameStart == 1) {
                       //DIALOG.
                       ProgressDialog dialog1 = ProgressDialog.show(WaitingRoom.this, "",
                               "Loading game. Please wait...", true);

                       new CountDownTimer(10000, 1000) {

                           public void onTick(long millisUntilFinished) {
                               endCountdown.setText("" + millisUntilFinished / 1000);
                           }

                           public void onFinish() {
                               // go to PlayerDetails screen
                               Intent myIntent = new Intent(WaitingRoom.this, InteractGAME.class);
                               startActivity(myIntent);
                           }
                       }.start();

                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.print("ERROR");
            }
        });


    }

}
