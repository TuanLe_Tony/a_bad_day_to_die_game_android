package com.example.abaddaytodie;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Enemy {
    FirebaseDatabase database;
    DatabaseReference root;

    int xPosition;
    int yPosition;
    int direction = -1;
    Bitmap image;
    int Speed = 15;

    private Rect hitBox;

    public Enemy(Context context, int x, int y) {

        database = FirebaseDatabase.getInstance();
        root = database.getReference();

        // Fetch particular child -> child -> etc =.=
        final DatabaseReference Players = FirebaseDatabase.getInstance().getReference("GAME1");


        Players.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot zoneSnapshot: dataSnapshot.getChildren()) {
                    Integer theY = zoneSnapshot.getValue(Integer.class);

                    //set data from database to game position
                    yPosition = theY;
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.print("ERROR");
            }
        });

        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.shipright);
        this.xPosition = x;
        this.yPosition = y;
        this.hitBox = new Rect(this.xPosition, this.yPosition, this.xPosition + this.image.getWidth(), this.yPosition + this.image.getHeight());
    }

    public void updateEnemyPosition() {
        if (this.direction == 0) {
            Speed = 15;
            // move down
            this.yPosition = this.yPosition - Speed;
            root.child("GAME1").child("LEFT").setValue(this.yPosition = this.yPosition - Speed);
        }
        else if (this.direction == 1) {
            Speed = 15;
            // move up
            this.yPosition = this.yPosition + Speed;
            root.child("GAME1").child("LEFT").setValue(this.yPosition = this.yPosition + Speed);
        }
        else if (this.direction == 2){
            Speed = 0;
        }
        // update the position of the hitbox
        this.updateHitbox();

    }

    public void updateHitbox() {
        // update the position of the hitbox
        this.hitBox.top = this.yPosition;
        this.hitBox.left = this.xPosition;
        this.hitBox.right = this.xPosition + this.image.getWidth();
        this.hitBox.bottom = this.yPosition + this.image.getHeight();
    }

    public Rect getHitbox() {
        return this.hitBox;
    }


    public void setXPosition(int x) {
        this.xPosition = x;
        this.updateHitbox();
    }
    public void setYPosition(int y) {
        this.yPosition = y;
        this.updateHitbox();
    }
    public void setDirection(int i) {
        this.direction = i;
    }
    public int getXPosition() {
        return this.xPosition;
    }
    public int getYPosition() {
        return this.yPosition;
    }

    public Bitmap getBitmap() {
        return this.image;
    }

}