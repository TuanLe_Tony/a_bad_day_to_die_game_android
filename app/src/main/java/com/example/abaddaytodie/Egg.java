package com.example.abaddaytodie;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Egg{

    FirebaseDatabase database;
    DatabaseReference root;

    int xPosition;
    int yPosition;
    int direction = -1;          // 0 = down, 1 = up
    Bitmap egg;
    int Speed = 20;

    private Rect hitBox;

    public Egg(Context context, int x, int y) {
        database = FirebaseDatabase.getInstance();
        root = database.getReference();

        // Fetch particular child -> child -> etc =.=
        final DatabaseReference egg1 = FirebaseDatabase.getInstance().getReference("EGG");
        egg1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot zoneSnapshot: dataSnapshot.getChildren()) {
                    Integer egg0 = zoneSnapshot.getValue(Integer.class);
                    //set data from database to game position
                    yPosition = egg0;
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.print("ERROR");
            }
        });

        this.egg = BitmapFactory.decodeResource(context.getResources(), R.drawable.eggright);
        this.xPosition = x;
        this.yPosition = y;
        this.hitBox = new Rect(this.xPosition, this.yPosition, this.xPosition + this.egg.getWidth(), this.yPosition + this.egg.getHeight());
    }
    public void updateEggPosition() {
        if (this.direction == 1){
            this.yPosition = this.yPosition - Speed;
            root.child("EGG").child("RIGHT").setValue(this.yPosition = this.yPosition - Speed);
        }
        // update the position of the hitbox
        this.updateHitbox();
    }

    public void updateHitbox() {
        // update the position of the hitbox
        this.hitBox.top = this.yPosition;
        this.hitBox.left = this.xPosition;
        this.hitBox.right = this.xPosition + this.egg.getWidth();
        this.hitBox.bottom = this.yPosition + this.egg.getHeight();
    }

    public Rect getHitbox() {
        return this.hitBox;
    }

    public void setXPosition(int x) {
        this.xPosition = x;
        this.updateHitbox();
    }
    public void setYPosition(int y) {
        this.yPosition = y;
        this.updateHitbox();
    }
    public int getXPosition() {
        return this.xPosition;
    }
    public int getYPosition() {
        return this.yPosition;
    }

    public void setDirection(int i) {
        this.direction = i;
    }
    public Bitmap getBitmap() {
        return this.egg;
    }

}
