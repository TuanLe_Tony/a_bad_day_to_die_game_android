package com.example.abaddaytodie;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class MathGame extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference root;


    EditText AnswerInput;
    TextView result1,result2,result3,notifycation,countdown, endCountdown;
    ImageView img1, img2, img3, img4, img5,img6,img7,img8,img9,question;

    private String randomImage[] =
            {"oranger", "pear", "strawberry", "wetermelon"};
    private String randomImage2[] =
            {"cat", "chicken", "dog", "mouse"};
    private String randomImage3[] =
            {"rocket", "bicycle", "unknownbox", "trap", "goose"};


    List<Integer> numbers = new ArrayList<Integer>();
    List<Integer> answers = new ArrayList<Integer>();



    Integer sum = 0;
    Integer sum1 = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_math_game);

        // database
        database = FirebaseDatabase.getInstance();
        root = database.getReference();

        //GET NAME THAT WAS SAVED FROM MainActivity :)
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor Edit = pref.edit();

        final String name = pref.getString("NAME", "NOTHING");         // getting String


        img1 = (ImageView)findViewById(R.id.Image1);
        img2 = (ImageView)findViewById(R.id.Image2);
        img3 = (ImageView)findViewById(R.id.Image3);
        img4 = (ImageView)findViewById(R.id.Image4);
        img5 = (ImageView)findViewById(R.id.Image5);
        img6 = (ImageView)findViewById(R.id.Image6);
        img7 = (ImageView)findViewById(R.id.Image7);
        img8 = (ImageView)findViewById(R.id.Image8);
        img9 = (ImageView)findViewById(R.id.Image9);
        question = (ImageView)findViewById(R.id.question);
        notifycation = (TextView)findViewById(R.id.Notify);
        countdown = (TextView) findViewById(R.id.countdown);
        endCountdown = (TextView) findViewById(R.id.endgame);


        result1 = (TextView)findViewById(R.id.result1);
        result2 = (TextView)findViewById(R.id.result2);
        result3 = (TextView)findViewById(R.id.result3);

        AnswerInput = (EditText)findViewById(R.id.answer);


        String random = (randomImage[new Random().nextInt(randomImage.length)]);
        String random2 = (randomImage2[new Random().nextInt(randomImage2.length)]);
        String random3 = (randomImage3[new Random().nextInt(randomImage3.length)]);

        // LINE 1
       if (random == "oranger") {
            img1.setImageResource(R.drawable.oranger);
            img2.setImageResource(R.drawable.oranger);
            img3.setImageResource(R.drawable.oranger);
            result1.setText("15");

        }
        else if (random == "strawberry") {
            img1.setImageResource(R.drawable.strawberry);
            img2.setImageResource(R.drawable.strawberry);
            img3.setImageResource(R.drawable.strawberry);
            result1.setText("30");

        }
        else if (random == "wetermelon") {
            img1.setImageResource(R.drawable.wetermelon);
            img2.setImageResource(R.drawable.wetermelon);
            img3.setImageResource(R.drawable.wetermelon);
            result1.setText("24");

        }
        else if (random == "pear") {
            img1.setImageResource(R.drawable.pear);
            img2.setImageResource(R.drawable.pear);
            img3.setImageResource(R.drawable.pear);
            result1.setText("90");

        }
        else {
            Log.d("Tony","Got nothing");
        }
        //LINE 2

        // FIRST Image - USE randomImage
         if (random == "oranger") {
            img4.setImageResource(R.drawable.oranger);
            numbers.add(5);
        }
        else if (random == "strawberry") {
            img4.setImageResource(R.drawable.strawberry);
            numbers.add(10);

        }
        else if (random == "wetermelon") {
            img4.setImageResource(R.drawable.wetermelon);
            numbers.add(8);


        }
        else if (random == "pear") {
            img4.setImageResource(R.drawable.pear);
            numbers.add(30);

        }
        else {
            Log.d("Tony","Got nothing");
        }

        // SECOND Image - USE randomImage2
        if (random == "oranger") {
            img5.setImageResource(R.drawable.oranger);
            numbers.add(5);
        }
        else if (random == "strawberry") {
            img5.setImageResource(R.drawable.strawberry);
            numbers.add(10);
        }
        else if (random == "wetermelon") {
            img5.setImageResource(R.drawable.wetermelon);
            numbers.add(8);
        }
        else if (random == "pear") {
            img5.setImageResource(R.drawable.pear);
            numbers.add(30);
        }
        else {
            Log.d("Tony","Got nothing");
        }

        // THIRD IMAGE

        // SECOND Image - USE randomImage3
        if (random2 == "cat") {
            img6.setImageResource(R.drawable.cat);
            numbers.add(15);
            //check if fruit was picked from previous line than change it to something else.

        } else if (random2 == "mouse") {
            img6.setImageResource(R.drawable.mouse);
            numbers.add(2);


        } else if (random2 == "dog") {
            img6.setImageResource(R.drawable.dog);
            numbers.add(40);

        } else if (random2 == "chicken") {
            img6.setImageResource(R.drawable.chicken);
            numbers.add(3);

        }
        else {
            Log.d("Tony", "Got nothing");
        }


        //LINE 3

        // FIRST Image - USE randomImage
         if (random == "oranger") {
            img7.setImageResource(R.drawable.oranger);
            answers.add(5);

        }
        else if (random == "strawberry") {
            img7.setImageResource(R.drawable.strawberry);
            answers.add(10);
        }
        else if (random == "wetermelon") {
            img7.setImageResource(R.drawable.wetermelon);
            answers.add(8);

        }
        else if (random == "pear") {
            img7.setImageResource(R.drawable.pear);
            answers.add(30);

        }
        else {
            Log.d("Tony","Got nothing");
        }

        // SECOND Image - USE randomImage2
        if (random2 == "cat") {
            img8.setImageResource(R.drawable.cat);
            answers.add(15);


        }
        else if (random2 == "mouse") {
            img8.setImageResource(R.drawable.mouse);
            answers.add(2);
        }
        else if (random2 == "dog") {
            img8.setImageResource(R.drawable.dog);
            answers.add(40);
        }
        else if (random2 == "chicken") {
            img8.setImageResource(R.drawable.chicken);
            answers.add(3);
        }
        else {
            Log.d("Tony","Got nothing");
        }

        // THIRD IMAGE

        // SECOND Image - USE randomImage3
        if (random3 == "rocket") {
            img9.setImageResource(R.drawable.rocket);
            question.setImageResource(R.drawable.rocket);
            answers.add(4);

        } else if (random3 == "goose") {
            img9.setImageResource(R.drawable.goose);
            question.setImageResource(R.drawable.goose);
            answers.add(2);

        } else if (random3 == "bicycle") {
            img9.setImageResource(R.drawable.bicycle);
            question.setImageResource(R.drawable.bicycle);
            answers.add(1);

        } else if (random3 == "unknownbox") {
            img9.setImageResource(R.drawable.unknownbox);
            question.setImageResource(R.drawable.unknownbox);
            answers.add(6);

        } else if (random3 == "trap") {
            img9.setImageResource(R.drawable.trap);
            question.setImageResource(R.drawable.trap);
            answers.add(7);
        } else {
            Log.d("Tony", "Got nothing");
        }



        Log.d("THIS THIS AN ARRAY", String.valueOf(numbers));
        Log.d("THIS THIS AN ARRAY", String.valueOf(answers));




        //CACULATED AND DISPLAY RESULT OF LINE 2 Display

        for (int i = 0; i < numbers.size(); i++)
        {
            sum += Integer.valueOf(numbers.get(i));
        }
        //set text :)
        result2.setText(String.valueOf(sum));
        Log.d("Answer line 2",String.valueOf(sum));

        //CACULATED AND DISPLAY RESULT OF LINE 3 Display
        for (int i = 0; i < answers.size(); i++)
        {
            sum1 += Integer.valueOf(answers.get(i));
        }
        //set text :)
        result3.setText(String.valueOf(sum1));
        Log.d("Answer line 3",String.valueOf(sum1));


        //click OK check answer
        Button btn = (Button)findViewById(R.id.OKbutton);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //DIALOG.
//                ProgressDialog dialog = ProgressDialog.show(MathGame.this, "",
//                        "Loading. Please wait...", true);

                final String crAnswer = AnswerInput.getText().toString();
                if (crAnswer.isEmpty()) {
                    Log.d("Tuan", "KO dc dau");
                    Toast.makeText(MathGame.this, "Answer empty!.", Toast.LENGTH_SHORT).show();
                } else {

                    //DIALOG.
                    ProgressDialog dialog = ProgressDialog.show(MathGame.this, "",
                            "Loading. Please wait...", true);

                    Log.d("Answer = ", String.valueOf(answers.get(2)) + "INPUT " + crAnswer);
                    if (Integer.valueOf(answers.get(2)) == Integer.valueOf(crAnswer)) {
                        notifycation.setText("YOU WON");

                        //GET SCORE THAT WAS SAVED FROM PlayerDetails :)
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                        SharedPreferences.Editor Edit = pref.edit();
                        String score = pref.getString("SCORE", "NOTHING");         // getting String
                        //UPDATE NEW SCORE
                        Integer updateScore = (Integer.valueOf(score) + 10);

                        //add NEW SCORE to database
                        root.child("Players").child(name).child("Score").setValue(String.valueOf(updateScore));

                        //3 secound countdown
                        new CountDownTimer(3000, 1000) {

                            public void onTick(long millisUntilFinished) {
                                endCountdown.setText("" + millisUntilFinished / 1000);
                            }

                            public void onFinish() {
                                // go to PlayerDetails screen
                                Intent myIntent = new Intent(MathGame.this, PlayerDetails.class);
                                startActivity(myIntent);
                            }
                        }.start();
                    } else {
                        //DIALOG.
                        ProgressDialog dialog1 = ProgressDialog.show(MathGame.this, "",
                                "Loading. Please wait...", true);

                        notifycation.setText("YOU LOST");
                        new CountDownTimer(3000, 1000) {

                            public void onTick(long millisUntilFinished) {
                                endCountdown.setText("" + millisUntilFinished / 1000);
                            }

                            public void onFinish() {
                                // go to PlayerDetails screen
                                Intent myIntent = new Intent(MathGame.this, Dealth.class);
                                startActivity(myIntent);
                            }
                        }.start();
                    }

                }
            }
            });

        //30 secound countdown
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                countdown.setText("" + millisUntilFinished / 1000);
            }

            public void onFinish() {
                // go to PlayerDetails screen
                Intent myIntent = new Intent(MathGame.this, Dealth.class);
                startActivity(myIntent);
            }
        }.start();

    }

    //exit from app when press back button
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        System.exit(0);
                    }
                }).setNegativeButton("No", null).show();
    }


}
